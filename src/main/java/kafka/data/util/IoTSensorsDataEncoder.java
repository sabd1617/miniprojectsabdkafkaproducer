package kafka.data.util;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import kafka.data.vo.IoTSensorsData;
import kafka.serializer.Encoder;
import kafka.utils.VerifiableProperties;

public class IoTSensorsDataEncoder implements Encoder<IoTSensorsData> {
	
	private static final Logger logger = Logger.getLogger(IoTSensorsDataEncoder.class);	
	private static ObjectMapper objectMapper = new ObjectMapper();		
	public IoTSensorsDataEncoder(VerifiableProperties verifiableProperties) {

    }
	public byte[] toBytes(IoTSensorsData iotEvent) {
		try {
			String msg = objectMapper.writeValueAsString(iotEvent);
			logger.info(msg);
			return msg.getBytes();
		} catch (JsonProcessingException e) {
			logger.error("Error in Serialization", e);
		}
		return null;
	}
}
