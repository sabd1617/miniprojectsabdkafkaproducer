package kafka.data.producer;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.log4j.Logger;

import kafka.data.util.PropertyFileReader;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

/**
 * Classe principale del producer di kafka. Il producer legge linea per linea il file di testo e lo
 * invia al job di flink che processa i dati.
 * 
 * @author falberto
 *
 */
public class IoTSensorsDataProducer {

	private static final Logger logger = Logger.getLogger(IoTSensorsDataProducer.class);
	private static final String inputFile = "/home/falberto/Scrivania/miniProgettoSABD2/"
	  		+ "full-game";
	
	public static void main(String[] args) throws Exception {
		
		Properties prop = PropertyFileReader.readPropertyFile();		
		String zookeeper = prop.getProperty("com.iot.app.kafka.zookeeper");
		String brokerList = prop.getProperty("com.iot.app.kafka.brokerlist");
		String topic = prop.getProperty("com.iot.app.kafka.topic");
		
		logger.info("Using Zookeeper=" + zookeeper + " ,Broker-list=" + brokerList + " and topic " + topic);

		Properties properties = new Properties();
		properties.put("zookeeper.connect", zookeeper);
		properties.put("metadata.broker.list", brokerList);
		properties.put("request.required.acks", "1");
		properties.put("serializer.class", "kafka.serializer.StringEncoder");
		Producer<String, String> producer = 
				new Producer<String, String>(new ProducerConfig(properties));
		IoTSensorsDataProducer iotProducer = new IoTSensorsDataProducer();
		iotProducer.generateSensorsIoTEvent(producer,topic);		
	}


	private void generateSensorsIoTEvent(Producer<String, String> producer, 
			String topic) throws InterruptedException {
		logger.info("Sending events");
		
		try{
	    	  FileInputStream fstream = new FileInputStream(inputFile);
	    	  DataInputStream in = new DataInputStream(fstream);
	    	  BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    	  String strLine = new String();
	    	 	while ((strLine = br.readLine()) != null) {
					  
					KeyedMessage<String, String> data = 
							new KeyedMessage<String, String>(topic, strLine);
					
					producer.send(data);
				}
	  	    in.close();
		
  	     }catch (Exception e){//Catch exception if any
  	     System.err.println("Error: " + e.getMessage());
  	    }
	}
	
}
