package kafka.data.vo;

import java.io.Serializable;

// sid, ts, x, y, z, |v|, |a|, vx, vy, vz, ax, ay, az
public class IoTSensorsData implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String sid;
	private long ts;
	private int x;
	private int y;
	private int z;
	private int modV;
	private int modA;
	private int vx;
	private int vy;
	private int vz;
	private int ax;
	private int ay;
	private int az;
	
	
	
	public IoTSensorsData() {
		super();
	}
	public IoTSensorsData(String sid, long ts, int x, int y, int z, int modV, int modA, int vx, int vy, int vz, int ax,
			int ay, int az) {
		super();
		this.sid = sid;
		this.ts = ts;
		this.x = x;
		this.y = y;
		this.z = z;
		this.modV = modV;
		this.modA = modA;
		this.vx = vx;
		this.vy = vy;
		this.vz = vz;
		this.ax = ax;
		this.ay = ay;
		this.az = az;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public long getTs() {
		return ts;
	}
	public void setTs(long ts) {
		this.ts = ts;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getZ() {
		return z;
	}
	public void setZ(int z) {
		this.z = z;
	}
	public int getModV() {
		return modV;
	}
	public void setModV(int modV) {
		this.modV = modV;
	}
	public int getModA() {
		return modA;
	}
	public void setModA(int modA) {
		this.modA = modA;
	}
	public int getVx() {
		return vx;
	}
	public void setVx(int vx) {
		this.vx = vx;
	}
	public int getVy() {
		return vy;
	}
	public void setVy(int vy) {
		this.vy = vy;
	}
	public int getVz() {
		return vz;
	}
	public void setVz(int vz) {
		this.vz = vz;
	}
	public int getAx() {
		return ax;
	}
	public void setAx(int ax) {
		this.ax = ax;
	}
	public int getAy() {
		return ay;
	}
	public void setAy(int ay) {
		this.ay = ay;
	}
	public int getAz() {
		return az;
	}
	public void setAz(int az) {
		this.az = az;
	}
	
	
}
